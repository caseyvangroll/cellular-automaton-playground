'use client';
import { createContext, useContext } from 'react';
import { useGrid } from './reducer';
import { GridStateType, ColorType, RuleType, Tool } from '@/types';

export type GridContextType = GridStateType & {
  setHeight: (height: number) => void;
  setWidth: (width: number) => void;
  setCellColors: (cellColors: ColorType[]) => void;
  step: (rules: RuleType[]) => void;
  tool: Tool;
  toolColor: ColorType;
  setToolColor: (nextColor: ColorType) => void;
  handleToolCellClick: (e: MouseEvent<HTMLDivElement, MouseEvent>) => void;
};

const GridContext = createContext<GridContextType | null>(null);

export function useGridContext(): GridContextType {
  const context = useContext(GridContext);
  if (!context) {
    throw new Error('useGridContext must be used within GridProvider');
  }
  return context;
}

export function GridProvider({ children }: React.PropsWithChildren) {
  const providerValue = useGrid();

  return (
    <GridContext.Provider value={providerValue}>
      {children}
    </GridContext.Provider>
  );
}
