'use client';
import {
  ColorType,
  GridStateType,
  RuleType,
  Tool,
  VisualRuleType,
} from '@/types';
import { useReducer, useCallback, useState } from 'react';
import {
  defaultGridHeight,
  defaultGridWidth,
  defaultColor,
  Rainbow,
  Black,
} from '@/constants';

export enum GridActionEnum {
  SET_GRID_HEIGHT = 'SET_GRID_HEIGHT',
  SET_GRID_WIDTH = 'SET_GRID_WIDTH',
  SET_CELL_COLOR = 'SET_CELL_COLOR',
  SET_CELL_COLORS = 'SET_CELL_COLORS',
  STEP = 'STEP',
}

export type SetGridHeightActionType = {
  type: GridActionEnum.SET_GRID_HEIGHT;
  height: number;
};
export type SetGridWidthActionType = {
  type: GridActionEnum.SET_GRID_WIDTH;
  width: number;
};
export type SetCellColorActionType = {
  type: GridActionEnum.SET_CELL_COLOR;
  cellIndex: number;
  cellColor: ColorType;
};
export type SetCellColorsActionType = {
  type: GridActionEnum.SET_CELL_COLORS;
  cellColors: ColorType[];
};
export type StepActionType = {
  type: GridActionEnum.STEP;
  rules: RuleType[];
};

export type GridActionType =
  | SetCellColorActionType
  | SetCellColorsActionType
  | SetGridHeightActionType
  | SetGridWidthActionType
  | StepActionType;

function handleSetGridHeight(
  state: GridStateType,
  action: SetGridHeightActionType
) {
  const nextCellColors = [];
  for (let i = 0; i < action.height * state.width; i += 1) {
    nextCellColors.push(state.cellColors[i] || defaultColor);
  }
  return { ...state, cellColors: nextCellColors, height: action.height };
}

function handleSetGridWidth(
  state: GridStateType,
  action: SetGridWidthActionType
) {
  const nextCellColors = [];
  for (let i = 0; i < state.height; i += 1) {
    nextCellColors.push(
      ...state.cellColors.slice(
        i * state.width,
        i * state.width + Math.min(state.width, action.width)
      )
    );

    if (action.width > state.width) {
      nextCellColors.push(
        ...new Array(action.width - state.width)
          .fill(null)
          .map(() => ({ ...defaultColor }))
      );
    }
  }
  return { ...state, cellColors: nextCellColors, width: action.width };
}

function areMatchingColors(
  colorA: ColorType | null,
  colorB: ColorType | null
): boolean {
  if (colorA === null || colorB === null) {
    return false;
  }

  return colorA.id === colorB.id || [colorA.id, colorB.id].includes(Rainbow.id);
}

type AdjacentCellColors = {
  northwest: ColorType | null;
  north: ColorType | null;
  northeast: ColorType | null;
  west: ColorType | null;
  east: ColorType | null;
  southwest: ColorType | null;
  south: ColorType | null;
  southeast: ColorType | null;
};
export function isRuleTriggeredForCell(
  cellColor: ColorType,
  adjacentCellColors: AdjacentCellColors,
  rule: RuleType
): boolean {
  if ('conditions' in rule) {
    const { color, conditions } = rule;
    const neighborCellColors = Object.values(adjacentCellColors);
    const result =
      areMatchingColors(cellColor, color) &&
      conditions.every((condition) => {
        return (
          neighborCellColors.filter((c) =>
            areMatchingColors(c, condition.neighborColor)
          ).length >= condition.neighborCt
        );
      });
    return result;
  }

  const { colorGrid } = rule as VisualRuleType;
  return (
    areMatchingColors(colorGrid[0], adjacentCellColors.northwest) &&
    areMatchingColors(colorGrid[1], adjacentCellColors.north) &&
    areMatchingColors(colorGrid[2], adjacentCellColors.northeast) &&
    areMatchingColors(colorGrid[3], adjacentCellColors.west) &&
    areMatchingColors(colorGrid[4], cellColor) &&
    areMatchingColors(colorGrid[5], adjacentCellColors.east) &&
    areMatchingColors(colorGrid[6], adjacentCellColors.southwest) &&
    areMatchingColors(colorGrid[7], adjacentCellColors.south) &&
    areMatchingColors(colorGrid[8], adjacentCellColors.southeast)
  );
}

function getNextCellColor(
  cellColor: ColorType,
  adjacentCellColors: AdjacentCellColors,
  rules: RuleType[]
) {
  for (let i = 0; i < rules.length; i += 1) {
    const rule = rules[i];
    if (isRuleTriggeredForCell(cellColor, adjacentCellColors, rule)) {
      return rule.nextColor;
    }
  }

  return cellColor;
}

export function getAdjacentCellColors(
  cellColors: ColorType[],
  cellIndex: number,
  width: number
): AdjacentCellColors {
  return {
    northwest:
      cellIndex % width === 0
        ? null
        : cellColors[cellIndex - width - 1] ?? null,
    north: cellColors[cellIndex - width] ?? null,
    northeast:
      cellIndex % width === width - 1
        ? null
        : cellColors[cellIndex - width + 1] ?? null,
    west: cellIndex % width === 0 ? null : cellColors[cellIndex - 1],
    east: cellIndex % width === width - 1 ? null : cellColors[cellIndex + 1],
    southwest:
      cellIndex % width === 0
        ? null
        : cellColors[cellIndex + width - 1] ?? null,
    south: cellColors[cellIndex + width] ?? null,
    southeast:
      cellIndex % width === width - 1
        ? null
        : cellColors[cellIndex + width + 1] ?? null,
  };
}

function handleStep(state: GridStateType, rules: RuleType[]): GridStateType {
  return {
    ...state,
    cellColors: state.cellColors.map((cellColor, cellIndex) => {
      const adjacentCellColors = getAdjacentCellColors(
        state.cellColors,
        cellIndex,
        state.width
      );
      return getNextCellColor(cellColor, adjacentCellColors, rules);
    }),
  };
}

function gridReducer(state: GridStateType, action: GridActionType) {
  switch (action.type) {
    case GridActionEnum.SET_GRID_HEIGHT:
      return handleSetGridHeight(state, action);
    case GridActionEnum.SET_GRID_WIDTH:
      return handleSetGridWidth(state, action);
    case GridActionEnum.SET_CELL_COLORS: {
      return {
        ...state,
        cellColors: action.cellColors,
      };
    }
    case GridActionEnum.SET_CELL_COLOR: {
      return {
        ...state,
        cellColors: [
          ...state.cellColors.slice(0, action.cellIndex),
          action.cellColor,
          ...state.cellColors.slice(action.cellIndex + 1),
        ],
      };
    }
    case GridActionEnum.STEP: {
      return handleStep(state, action.rules);
    }
  }
}

function getInitialGridState(
  width: number = defaultGridWidth,
  height: number = defaultGridHeight,
  cellColors: ColorType[] = new Array(width * height)
    .fill(null)
    .map(() => ({ ...defaultColor }))
): GridStateType {
  return {
    width,
    height,
    cellColors,
  };
}

export function useGrid(initialState: GridStateType = getInitialGridState()) {
  const [grid, dispatch] = useReducer(gridReducer, initialState);

  const setHeight = useCallback(
    (height: number) =>
      dispatch({ type: GridActionEnum.SET_GRID_HEIGHT, height }),
    []
  );

  const setWidth = useCallback(
    (width: number) => dispatch({ type: GridActionEnum.SET_GRID_WIDTH, width }),
    []
  );

  const setCellColors = useCallback(
    (cellColors: ColorType[]) =>
      dispatch({ type: GridActionEnum.SET_CELL_COLORS, cellColors }),
    []
  );

  const step = useCallback(
    (rules: RuleType[]) => dispatch({ type: GridActionEnum.STEP, rules }),
    []
  );

  const [tool, setTool] = useState<Tool>('Pen');
  const [toolColor, setToolColor] = useState(Black);
  const handleToolCellClick = useCallback(
    (e: MouseEvent<HTMLDivElement, MouseEvent>) => {
      const cellIndex = parseInt(e.target?.dataset?.index);
      if (tool === 'Pen' && !isNaN(cellIndex)) {
        console.log('dispatch');
        dispatch({
          type: GridActionEnum.SET_CELL_COLOR,
          cellColor: toolColor,
          cellIndex,
        });
      }
    },
    [tool, toolColor]
  );
  return {
    ...grid,
    setHeight,
    setWidth,
    setCellColors,
    step,
    tool,
    setTool,
    toolColor,
    setToolColor,
    handleToolCellClick,
  };
}
