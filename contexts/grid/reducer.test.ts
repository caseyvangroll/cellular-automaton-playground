import { act, renderHook } from '@testing-library/react';
import {
  useGrid,
  getAdjacentCellColors,
  isRuleTriggeredForCell,
} from './reducer';
import {
  defaultColor,
  defaultGridHeight,
  defaultGridWidth,
  White,
  Red,
  Black,
  Blue,
} from '@/constants';
import { TextualRuleType, VisualRuleType } from '@/types';

const mockInitialState = {
  width: 3,
  height: 3,
  // prettier-ignore
  cellColors: [
    White, Red, Black,
    Blue, Red, Black,
    Black, White, Red
  ],
};

describe('isRuleTriggeredForCell', () => {
  test('should detect if textual rule is triggered', () => {
    const cellIndex = 4;
    const adjacentCellColors = getAdjacentCellColors(
      mockInitialState.cellColors,
      4,
      mockInitialState.width
    );
    const rule: TextualRuleType = {
      color: Red,
      conditions: [
        {
          neighborCt: 1,
          neighborColor: Black,
        },
        {
          neighborCt: 1,
          neighborColor: White,
        },
      ],
      nextColor: White,
    };
    const actual = isRuleTriggeredForCell(
      mockInitialState.cellColors[cellIndex],
      adjacentCellColors,
      rule
    );
    expect(actual).toEqual(true);
  });

  test('should detect if textual rule is not triggered', () => {
    const cellIndex = 4;
    const adjacentCellColors = getAdjacentCellColors(
      mockInitialState.cellColors,
      4,
      mockInitialState.width
    );
    const rule: TextualRuleType = {
      color: Red,
      conditions: [
        {
          neighborCt: 4,
          neighborColor: Black,
        },
      ],
      nextColor: White,
    };
    const actual = isRuleTriggeredForCell(
      mockInitialState.cellColors[cellIndex],
      adjacentCellColors,
      rule
    );
    expect(actual).toEqual(false);
  });

  test('should detect if visual rule is triggered', () => {
    const cellIndex = 4;
    const adjacentCellColors = getAdjacentCellColors(
      mockInitialState.cellColors,
      4,
      mockInitialState.width
    );
    const rule: VisualRuleType = {
      colorGrid: mockInitialState.cellColors,
      nextColor: White,
    };
    const actual = isRuleTriggeredForCell(
      mockInitialState.cellColors[cellIndex],
      adjacentCellColors,
      rule
    );
    expect(actual).toEqual(true);
  });

  test('should detect if visual rule is not triggered', () => {
    const cellIndex = 4;
    const adjacentCellColors = getAdjacentCellColors(
      mockInitialState.cellColors,
      4,
      mockInitialState.width
    );
    const rule: VisualRuleType = {
      // prettier-ignore
      colorGrid: [
        Red, Red, Red,
        Red, Red, Red,
        Red, Red, Red
      ],
      nextColor: White,
    };
    const actual = isRuleTriggeredForCell(
      mockInitialState.cellColors[cellIndex],
      adjacentCellColors,
      rule
    );
    expect(actual).toEqual(false);
  });
});

describe('getAdjacentCellColors', () => {
  test('should work for middle of grid', () => {
    const actual = getAdjacentCellColors(
      mockInitialState.cellColors,
      4,
      mockInitialState.width
    );
    expect(actual).toEqual({
      northwest: White,
      north: Red,
      northeast: Black,
      west: Blue,
      east: Black,
      southwest: Black,
      south: White,
      southeast: Red,
    });
  });

  test('should work for top left corner of grid', () => {
    const actual = getAdjacentCellColors(
      mockInitialState.cellColors,
      0,
      mockInitialState.width
    );
    expect(actual).toEqual({
      northwest: null,
      north: null,
      northeast: null,
      west: null,
      east: Red,
      southwest: null,
      south: Blue,
      southeast: Red,
    });
  });

  test('should work for top right corner of grid', () => {
    const actual = getAdjacentCellColors(
      mockInitialState.cellColors,
      2,
      mockInitialState.width
    );
    expect(actual).toEqual({
      northwest: null,
      north: null,
      northeast: null,
      west: Red,
      east: null,
      southwest: Red,
      south: Black,
      southeast: null,
    });
  });

  test('should work for bottom left corner of grid', () => {
    const actual = getAdjacentCellColors(
      mockInitialState.cellColors,
      6,
      mockInitialState.width
    );
    expect(actual).toEqual({
      northwest: null,
      north: Blue,
      northeast: Red,
      west: null,
      east: White,
      southwest: null,
      south: null,
      southeast: null,
    });
  });

  test('should work for bottom right corner of grid', () => {
    const actual = getAdjacentCellColors(
      mockInitialState.cellColors,
      8,
      mockInitialState.width
    );
    expect(actual).toEqual({
      northwest: Red,
      north: Black,
      northeast: null,
      west: White,
      east: null,
      southwest: null,
      south: null,
      southeast: null,
    });
  });
});

describe('useGrid', () => {
  describe('initial state', () => {
    test('should have expected defaults', () => {
      const { result } = renderHook(() => useGrid());
      expect(result.current.width).toEqual(defaultGridWidth);
      expect(result.current.height).toEqual(defaultGridHeight);
      // prettier-ignore
      expect(result.current.cellColors).toEqual([
        defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor,
        defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor,
        defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor,
        defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor,
        defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor,
        defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor,
        defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor,
        defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor,
        defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor,
        defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor, defaultColor,
      ]);
    });

    test('should use initial state argument if provided', () => {
      const { result } = renderHook(() => useGrid(mockInitialState));
      expect(result.current.width).toEqual(mockInitialState.width);
      expect(result.current.height).toEqual(mockInitialState.height);
      expect(result.current.cellColors).toEqual(mockInitialState.cellColors);
    });
  });

  describe('setHeight', () => {
    test('should set height in store', () => {
      const { result } = renderHook(() => useGrid(mockInitialState));

      act(() => {
        result.current.setHeight(5);
      });

      expect(result.current.height).toEqual(5);
    });

    test('should pad new cellColors with defaultColor if height is larger', () => {
      const { result } = renderHook(() => useGrid(mockInitialState));

      act(() => {
        result.current.setHeight(5);
      });

      // prettier-ignore
      const expectedCellColors = [
        White, Red, Black,
        Blue, Red, Black,
        Black, White, Red,
        defaultColor, defaultColor, defaultColor,
        defaultColor, defaultColor, defaultColor,
      ];
      expect(result.current.cellColors).toEqual(expectedCellColors);
    });

    test('should trim cellColors if height is smaller', () => {
      const { result } = renderHook(() => useGrid(mockInitialState));

      act(() => {
        result.current.setHeight(2);
      });
      // prettier-ignore
      const expectedCellColors = [
        White, Red, Black,
        Blue, Red, Black,
      ];
      expect(result.current.cellColors).toEqual(expectedCellColors);
    });
  });

  describe('setWidth', () => {
    test('should set width in store', () => {
      const { result } = renderHook(() => useGrid(mockInitialState));

      act(() => {
        result.current.setWidth(5);
      });

      expect(result.current.width).toEqual(5);
    });

    test('should pad new cellColors with defaultColor if width is larger', () => {
      const { result } = renderHook(() => useGrid(mockInitialState));

      act(() => {
        result.current.setWidth(5);
      });

      // prettier-ignore
      const expectedCellColors = [
        White, Red, Black, defaultColor, defaultColor,
        Blue, Red, Black, defaultColor, defaultColor,
        Black, White, Red, defaultColor, defaultColor,
      ];
      expect(result.current.cellColors).toEqual(expectedCellColors);
    });

    test('should trim cellColors if width is smaller', () => {
      const { result } = renderHook(() => useGrid(mockInitialState));

      act(() => {
        result.current.setWidth(2);
      });
      // prettier-ignore
      const expectedCellColors = [
        White, Red,
        Blue, Red,
        Black, White
      ];
      expect(result.current.cellColors).toEqual(expectedCellColors);
    });
  });

  describe('setCellColors', () => {
    test('should set cellColors in store', () => {
      const { result } = renderHook(() => useGrid(mockInitialState));

      // prettier-ignore
      const cellColors = [
        Black, Black, Black,
        Red, Red, Red,
        White, White, White
      ]

      act(() => {
        result.current.setCellColors(cellColors);
      });

      expect(result.current.cellColors).toEqual(cellColors);
    });
  });

  describe('step', () => {
    test('should be be no-op if no rules passed in', () => {
      const { result } = renderHook(() => useGrid(mockInitialState));

      act(() => {
        result.current.step([]);
      });

      expect(result.current.cellColors).toEqual(mockInitialState.cellColors);
    });

    test('should apply one textual rule correctly', () => {
      const { result } = renderHook(() => useGrid(mockInitialState));

      act(() => {
        result.current.step([]);
      });

      expect(result.current.cellColors).toEqual(mockInitialState.cellColors);
    });

    test('should apply two rules in order', () => {});
  });
});
