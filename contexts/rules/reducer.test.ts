import { act, renderHook } from '@testing-library/react';
import {
  useRules,
  getDefaultTextualRule,
  getDefaultVisualRule,
} from './reducer';
import { Direction } from '@/types';
import { defaultColor, colors } from '@/constants';

const mockTextualRule = {
  color: defaultColor,
  conditions: [],
  nextColor: defaultColor,
};

const mockVisualRule = {
  colorGrid: new Array(9).fill(null).map(() => defaultColor),
  nextColor: defaultColor,
};

const getMockInitialState = () => [
  { ...mockTextualRule },
  { ...mockVisualRule },
  { ...mockTextualRule },
];

describe('useRules', () => {
  describe('initial state', () => {
    test('should have default of []', () => {
      const { result } = renderHook(() => useRules());
      expect(result.current.rules).toEqual([]);
    });

    test('should use initial state argument if provided', () => {
      const initialState = getMockInitialState();
      const { result } = renderHook(() => useRules(initialState));
      expect(result.current.rules).toEqual(initialState);
    });
  });

  describe('addTextualRule', () => {
    test('should add textual rule to end of array', () => {
      const initialState = getMockInitialState();
      const { result } = renderHook(() => useRules(initialState));

      act(() => {
        result.current.addTextualRule();
      });

      const expected = [...initialState, getDefaultTextualRule()];
      expect(result.current.rules).toEqual(expected);
    });
  });

  describe('addVisualRule', () => {
    test('should add visual rule to end of array', () => {
      const initialState = getMockInitialState();
      const { result } = renderHook(() => useRules(initialState));

      act(() => {
        result.current.addVisualRule();
      });

      const expected = [...initialState, getDefaultVisualRule()];
      expect(result.current.rules).toEqual(expected);
    });
  });

  describe('moveRule', () => {
    test('should not move rule at start upwards', () => {
      const initialState = getMockInitialState();
      const { result } = renderHook(() => useRules(initialState));

      act(() => {
        result.current.moveRule(0, Direction.Up);
      });

      const expected = initialState;
      expect(result.current.rules).toEqual(expected);
    });

    test('should move rule at start downwards', () => {
      const initialState = getMockInitialState();
      const { result } = renderHook(() => useRules(initialState));

      act(() => {
        result.current.moveRule(0, Direction.Down);
      });

      const expected = [
        { ...mockVisualRule },
        { ...mockTextualRule },
        { ...mockTextualRule },
      ];
      expect(result.current.rules).toEqual(expected);
    });

    test('should move rule in middle downwards', () => {
      const initialState = getMockInitialState();
      const { result } = renderHook(() => useRules(initialState));

      act(() => {
        result.current.moveRule(1, Direction.Down);
      });

      const expected = [
        { ...mockTextualRule },
        { ...mockTextualRule },
        { ...mockVisualRule },
      ];
      expect(result.current.rules).toEqual(expected);
    });

    test('should move rule in middle upwards', () => {
      const initialState = getMockInitialState();
      const { result } = renderHook(() => useRules(initialState));

      act(() => {
        result.current.moveRule(1, Direction.Up);
      });

      const expected = [
        { ...mockVisualRule },
        { ...mockTextualRule },
        { ...mockTextualRule },
      ];
      expect(result.current.rules).toEqual(expected);
    });

    test('should move rule at end upwards', () => {
      const initialState = getMockInitialState();
      const { result } = renderHook(() => useRules(initialState));

      act(() => {
        result.current.moveRule(2, Direction.Up);
      });

      const expected = [
        { ...mockTextualRule },
        { ...mockTextualRule },
        { ...mockVisualRule },
      ];
      expect(result.current.rules).toEqual(expected);
    });

    test('should not move rule at end downwards', () => {
      const initialState = getMockInitialState();
      const { result } = renderHook(() => useRules(initialState));

      act(() => {
        result.current.moveRule(initialState.length - 1, Direction.Down);
      });

      const expected = initialState;
      expect(result.current.rules).toEqual(expected);
    });

    test('should be noop for invalid index', () => {
      const initialState = getMockInitialState();
      const { result } = renderHook(() => useRules(initialState));

      act(() => {
        result.current.moveRule(1000, Direction.Down);
      });

      const expected = initialState;
      expect(result.current.rules).toEqual(expected);
    });
  });

  describe('editRule', () => {
    test('should replace rule at index with textual rule', () => {
      const initialState = getMockInitialState();
      const { result } = renderHook(() => useRules(initialState));
      const newRule = { ...mockTextualRule, nextColor: colors[3] };

      act(() => {
        result.current.editRule(newRule, 1);
      });

      const expected = [initialState[0], newRule, initialState[2]];
      expect(result.current.rules).toEqual(expected);
    });

    test('should replace rule at index with visual rule', () => {
      const initialState = getMockInitialState();
      const { result } = renderHook(() => useRules(initialState));
      const newRule = { ...mockVisualRule, nextColor: colors[3] };

      act(() => {
        result.current.editRule(newRule, 1);
      });

      const expected = [initialState[0], newRule, initialState[2]];
      expect(result.current.rules).toEqual(expected);
    });
  });

  describe('removeRule', () => {
    test('should remove rule at index', () => {
      const initialState = getMockInitialState();
      const { result } = renderHook(() => useRules(initialState));

      act(() => {
        result.current.removeRule(1);
      });

      const expected = [initialState[0], initialState[2]];
      expect(result.current.rules).toEqual(expected);
    });
  });
});
