'use client';
import { createContext, useContext } from 'react';
import { useRules } from './reducer';
import { RuleType, Direction } from '@/types';

export type RulesContextType = {
  rules: RuleType[];
  setRules: (rules: RuleType[]) => void;
  addTextualRule: () => void;
  addVisualRule: () => void;
  moveRule: (index: number, direction: Direction) => void;
  editRule: (rule: RuleType, index: number) => void;
  removeRule: (index: number) => void;
};

const RulesContext = createContext<RulesContextType | null>(null);

export function useRulesContext(): RulesContextType {
  const context = useContext(RulesContext);
  if (!context) {
    throw new Error('useRulesContext must be used within RulesProvider');
  }
  return context;
}

export function RulesProvider({ children }: React.PropsWithChildren) {
  const providerValue = useRules();

  return (
    <RulesContext.Provider value={providerValue}>
      {children}
    </RulesContext.Provider>
  );
}
