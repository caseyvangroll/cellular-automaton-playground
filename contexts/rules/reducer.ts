'use client';
import { RuleType, TextualRuleType, VisualRuleType, Direction } from '@/types';
import { useReducer, useCallback } from 'react';
import {
  removeArrayElement,
  insertArrayElement,
  updateArrayElementInPlace,
} from '@/utils';
import { defaultColor } from '@/constants';

export enum RuleActionEnum {
  SET_RULES = 'SET_RULES',
  ADD_RULE = 'ADD_RULE',
  MOVE_RULE = 'MOVE_RULE',
  EDIT_RULE = 'EDIT_RULE',
  REMOVE_RULE = 'REMOVE_RULE',
}

export type SetRulesActionType = {
  type: RuleActionEnum.SET_RULES;
  rules: RuleType[];
};
export type AddRuleActionType = {
  type: RuleActionEnum.ADD_RULE;
  rule: RuleType;
};
export type MoveRuleActionType = {
  type: RuleActionEnum.MOVE_RULE;
  index: number;
  direction: Direction;
};
export type EditRuleActionType = {
  type: RuleActionEnum.EDIT_RULE;
  rule: RuleType;
  index: number;
};
export type RemoveRuleActionType = {
  type: RuleActionEnum.REMOVE_RULE;
  index: number;
};

export type RuleActionType =
  | SetRulesActionType
  | AddRuleActionType
  | MoveRuleActionType
  | EditRuleActionType
  | RemoveRuleActionType;

function rulesReducer(state: RuleType[], action: RuleActionType) {
  switch (action.type) {
    case RuleActionEnum.SET_RULES: {
      return action.rules;
    }
    case RuleActionEnum.ADD_RULE: {
      return [...state, action.rule];
    }
    case RuleActionEnum.MOVE_RULE: {
      const { index, direction } = action;
      if (
        index < 0 ||
        (index === 0 && direction === Direction.Up) ||
        (index === state.length - 1 && direction === Direction.Down) ||
        index > state.length
      ) {
        return state;
      }
      const delta = direction === Direction.Up ? -1 : 1;
      const targetIndex = index + delta;
      const ruleBeingMoved = { ...state[index] };
      return insertArrayElement(
        removeArrayElement(state, index),
        ruleBeingMoved,
        targetIndex
      );
    }
    case RuleActionEnum.EDIT_RULE: {
      return updateArrayElementInPlace(state, action.rule, action.index);
    }
    case RuleActionEnum.REMOVE_RULE: {
      return removeArrayElement(state, action.index);
    }
  }
}

export function getDefaultTextualRule(): TextualRuleType {
  return {
    color: defaultColor,
    conditions: [],
    nextColor: defaultColor,
  };
}

export function getDefaultVisualRule(): VisualRuleType {
  return {
    colorGrid: new Array(9).fill(null).map(() => defaultColor),
    nextColor: defaultColor,
  };
}

export function useRules(initialState: RuleType[] = []) {
  const [rules, dispatch] = useReducer(rulesReducer, initialState);

  const setRules = useCallback(
    (rules: RuleType[]) =>
      dispatch({
        type: RuleActionEnum.SET_RULES,
        rules,
      }),
    [dispatch]
  );

  const addTextualRule = useCallback(
    () =>
      dispatch({
        type: RuleActionEnum.ADD_RULE,
        rule: getDefaultTextualRule(),
      }),
    [dispatch]
  );

  const addVisualRule = useCallback(
    () =>
      dispatch({ type: RuleActionEnum.ADD_RULE, rule: getDefaultVisualRule() }),
    [dispatch]
  );

  const moveRule = useCallback(
    (index: number, direction: Direction) =>
      dispatch({ type: RuleActionEnum.MOVE_RULE, index, direction }),
    [dispatch]
  );

  const editRule = useCallback(
    (rule: RuleType, index: number) =>
      dispatch({ type: RuleActionEnum.EDIT_RULE, rule, index }),
    [dispatch]
  );

  const removeRule = useCallback(
    (index: number) => dispatch({ type: RuleActionEnum.REMOVE_RULE, index }),
    [dispatch]
  );

  return {
    rules,
    setRules,
    addTextualRule,
    addVisualRule,
    moveRule,
    editRule,
    removeRule,
  };
}
