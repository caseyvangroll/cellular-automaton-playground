import React from 'react';
import './globals.scss';
import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Cellular Automaton Laboratory',
  description:
    'Create and test 2-dimensional cellular automatons that operate on colors.',
};

export interface RootLayoutProps {
  children: React.ReactNode;
}

export default function RootLayout({ children }: RootLayoutProps) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
