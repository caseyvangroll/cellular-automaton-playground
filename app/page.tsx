import { Grid, Rules, Presets, Controls } from '@/components';
import { GridProvider, RulesProvider } from '@/contexts';

export default function RootPage() {
  return (
    <main>
      <RulesProvider>
        <GridProvider>
          <Presets />
          <div id="grid-rules-wrapper">
            <Grid />
            <Rules />
          </div>
          <Controls />
        </GridProvider>
      </RulesProvider>
    </main>
  );
}
