import {
  updateArrayElementInPlace,
  insertArrayElement,
  removeArrayElement,
} from './array';

const one = { id: '1' };
const two = { id: '2' };
const three = { id: '3' };
const four = { id: '4' };

describe('updateArrayElementInPlace', () => {
  const five = { id: '5' };

  test('should update element at start', () => {
    const array = [one, two, three, four];
    expect(updateArrayElementInPlace(array, five, 0)).toEqual([
      five,
      two,
      three,
      four,
    ]);
  });

  test('should update element in middle', () => {
    const array = [one, two, three, four];
    expect(updateArrayElementInPlace(array, five, 2)).toEqual([
      one,
      two,
      five,
      four,
    ]);
  });

  test('should update element at end', () => {
    const array = [one, two, three, four];
    expect(updateArrayElementInPlace(array, five, 3)).toEqual([
      one,
      two,
      three,
      five,
    ]);
  });

  test('should be noop for invalid index', () => {
    const array = [one, two, three, four];
    expect(updateArrayElementInPlace(array, five, 1000)).toEqual([
      one,
      two,
      three,
      four,
    ]);
  });
});

describe('insertArrayElement', () => {
  const five = { id: '5' };

  test('should insert element at start', () => {
    const array = [one, two, three, four];
    expect(insertArrayElement(array, five, 0)).toEqual([
      five,
      one,
      two,
      three,
      four,
    ]);
  });

  test('should insert element in middle', () => {
    const array = [one, two, three, four];
    expect(insertArrayElement(array, five, 2)).toEqual([
      one,
      two,
      five,
      three,
      four,
    ]);
  });

  test('should insert element at end', () => {
    const array = [one, two, three, four];
    expect(insertArrayElement(array, five, 4)).toEqual([
      one,
      two,
      three,
      four,
      five,
    ]);
  });

  test('should be noop for invalid index', () => {
    const array = [one, two, three, four];
    expect(insertArrayElement(array, five, 1000)).toEqual([
      one,
      two,
      three,
      four,
    ]);
  });
});

describe('removeArrayElement', () => {
  test('should remove element at start', () => {
    const array = [one, two, three, four];
    expect(removeArrayElement(array, 0)).toEqual([two, three, four]);
  });

  test('should remove element in middle', () => {
    const array = [one, two, three, four];
    expect(removeArrayElement(array, 2)).toEqual([one, two, four]);
  });

  test('should remove element at end', () => {
    const array = [one, two, three, four];
    expect(removeArrayElement(array, 3)).toEqual([one, two, three]);
  });

  test('should be noop for invalid index', () => {
    const array = [one, two, three, four];
    expect(removeArrayElement(array, 1000)).toEqual([one, two, three, four]);
  });
});
