// This file was created because of some noisy deprecation logging
// done by React in response to the react-color GithubPicker component.
// Use sparingly.

const actualConsoleError = console.error;
const muffledErrors = new Set();
export function muffleConsoleErrors(errorSignaturesToRemove: string[]) {
  errorSignaturesToRemove.forEach((errorSignature) =>
    muffledErrors.add(errorSignature)
  );
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const wrappedConsoleError = (...args: any[]) => {
  if (!Array.from(muffledErrors).some((s) => args[0]?.includes(s))) {
    actualConsoleError(...args);
  }
};

// Install once, only in browser
if (
  typeof window !== 'undefined' &&
  console.error === actualConsoleError &&
  process.env.NODE_ENV !== 'test'
) {
  console.error = wrappedConsoleError;
}
