export function updateArrayElementInPlace<T>(
  prevElements: Array<T>,
  updatedElement: T,
  indexOfUpdatedElement: number
): Array<T> {
  if (
    indexOfUpdatedElement < 0 ||
    indexOfUpdatedElement >= prevElements.length
  ) {
    return prevElements;
  }
  return [
    ...prevElements.slice(0, indexOfUpdatedElement),
    updatedElement,
    ...prevElements.slice(indexOfUpdatedElement + 1),
  ];
}

export function insertArrayElement<T>(
  prevElements: Array<T>,
  element: T,
  index: number
): Array<T> {
  if (index < 0 || index > prevElements.length) {
    return prevElements;
  }
  return [
    ...prevElements.slice(0, index),
    element,
    ...prevElements.slice(index),
  ];
}

export function removeArrayElement<T>(
  prevElements: Array<T>,
  indexOfElementToRemove: number
): Array<T> {
  return [
    ...prevElements.slice(0, indexOfElementToRemove),
    ...prevElements.slice(indexOfElementToRemove + 1),
  ];
}
