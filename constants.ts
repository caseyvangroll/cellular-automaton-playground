import { ColorType } from '@/types';

export const White = {
  id: 'White',
  hexValue: '#ffffff',
};
export const Black = {
  id: 'Black',
  hexValue: '#000000',
};
export const Red = {
  id: 'Red',
  hexValue: '#ff0000',
};
export const Orange = {
  id: 'Orange',
  hexValue: '#ff6600',
};
export const Yellow = {
  id: 'Yellow',
  hexValue: '#ffff00',
};
export const Green = {
  id: 'Green',
  hexValue: '#00ff00',
};
export const Blue = {
  id: 'Blue',
  hexValue: '#0000ff',
};
export const Purple = {
  id: 'Purple',
  hexValue: '#6600ff',
};
export const Brown = {
  id: 'Brown',
  hexValue: '#664422',
};
export const Rainbow = {
  id: 'Rainbow',
  hexValue: 'transparent',
};

export const colors: ColorType[] = [
  White,
  Black,
  Red,
  Orange,
  Yellow,
  Green,
  Blue,
  Purple,
  Brown,
  Rainbow,
];

export const defaultColor = White;
export const defaultGridHeight = 10;
export const defaultGridWidth = 10;
