# Cellular Automaton Laboratory

## Installation
### Requirements
- Node 20
- If using VSCode install recommended extensions

### Setup
- `npm install`
- If behind proxy:
  - Download [zipped cypress binary](https://www.cypress.io/)
  - Run `CYPRESS_INSTALL_BINARY=<zipped_binary_location> npm install`

## Automaton Rules

- You may define as many of them as you like (consider [regent](https://www.npmjs.com/package/regent) for running these under the hood)
- Rules will be evaluated sequentially until one evaluates to true for the current cell
- If no rule evaluates to true for the cell then it will retain its current color

### Definition
Rules will be definable 2 ways, each with different capabilities.

#### Visual Rules:
- These will allow you to specify exact configurations of neighbors (needed for definitions similar to those in [Rule 101](https://en.wikipedia.org/wiki/Rule_110#/media/File:One-d-cellular-automaton-rule-110.gif), except in 2d grid).
- A UI component will assist in creating these, you will be selecting colors from dropdowns in a 9x9 grid and then a next color for the center cell.

#### Textual Rules:
- These will allow you to specify general configurations of neighbors (needed for Conway's Game of Life).
- General Format: `If cell is {color}( and {N neighbors} are {color})* then cell changes to {color}`
  - The rule `{N neighbors} are {color}` indicates **at least** N neighbors are color, not **exactly** N neighbors are that color.
- A UI component will assist in creating these, you will not be entering text yourself - you will be clicking buttons and selecting dropdowns.

---

## Example Textual Rules for Conway's Game of Life:

- Each cell with one or no neighbors dies, as if by solitude.
  - `If cell is {yellow} and {7 neighbors} are {black} then cell changes to {black}`
- Each cell with four or more neighbors dies, as if by overpopulation.
  - `If cell is {yellow} and {4 neighbors} are {yellow} then cell changes to {black}`
- Each cell with two or three neighbors survives.
  - No rule needed - cells will retain color if no rules are true for them.
- For a space that is empty or unpopulated: Each cell with three neighbors becomes populated.
  - `If cell is {black} and {3 neighbors} are {yellow} and {5 neighbors} are {black} then cell changes to {yellow}`

## TODO
- Finish presets
