/** @type {import('next').NextConfig} */
const nextConfig = {
  eslint: {
    dirs: ['app', 'components', 'contexts', 'utils'],
  },
};

module.exports = nextConfig;
