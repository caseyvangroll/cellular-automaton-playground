'use client';
import { useGridContext } from '@/contexts';
import { ColorType } from '@/types';
import { Settings } from './settings';
import './index.scss';

interface CellsProps {
  height: number;
  width: number;
  cellColors: ColorType[];
  handleToolCellClick: (e: MouseEvent<HTMLDivElement, MouseEvent>) => void;
}
function Cells({ width, cellColors, handleToolCellClick }: CellsProps) {
  const gridStyle = {
    gridTemplateColumns: `repeat(${width}, 1fr)`,
  };
  return (
    <div className="cells" style={gridStyle}>
      {cellColors.map((color, index) => (
        <div
          key={index}
          className={color.id}
          onClick={handleToolCellClick}
          data-index={index}
          data-color-id={color.id}
        />
      ))}
    </div>
  );
}

export function Grid() {
  const {
    cellColors,
    handleToolCellClick,
    height,
    setHeight,
    width,
    setWidth,
  } = useGridContext();

  return (
    <div id="grid">
      <div className="section-header">
        <h3>Grid</h3>
        <Settings
          height={height}
          setHeight={setHeight}
          width={width}
          setWidth={setWidth}
        />
      </div>
      <Cells
        height={height}
        width={width}
        cellColors={cellColors}
        handleToolCellClick={handleToolCellClick}
      />
    </div>
  );
}
