'use client';
import { memo, useCallback } from 'react';

const heightOptions = ['10', '20', '30'];
const widthOptions = ['10', '20', '30'];

interface SettingsProps {
  height: number;
  width: number;
  setHeight: (height: number) => void;
  setWidth: (width: number) => void;
}

export function SettingsBase({
  height,
  width,
  setHeight,
  setWidth,
}: SettingsProps) {
  const handleSetHeight = useCallback(
    (event: React.ChangeEvent<HTMLSelectElement>) => {
      setHeight(parseInt(event.target.value, 10));
    },
    [setHeight]
  );

  const handleSetWidth = useCallback(
    (event: React.ChangeEvent<HTMLSelectElement>) => {
      setWidth(parseInt(event.target.value, 10));
    },
    [setWidth]
  );
  return (
    <>
      <label>
        Height
        <select
          value={height}
          onChange={handleSetHeight}
          data-testid="grid-height-select"
        >
          {heightOptions.map((value) => (
            <option key={value} value={value}>
              {value}
            </option>
          ))}
        </select>
      </label>
      <label>
        Width
        <select
          value={width}
          onChange={handleSetWidth}
          data-testid="grid-width-select"
        >
          {widthOptions.map((value) => (
            <option key={value} value={value}>
              {value}
            </option>
          ))}
        </select>
      </label>
    </>
  );
}

export const Settings = memo(SettingsBase);
