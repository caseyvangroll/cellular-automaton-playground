import { render, screen } from '@testing-library/react';
import { within } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { VisualRule } from './visual-rule';
import { colors, defaultColor } from '@/constants';
import { Direction } from '@/types';
import { getDefaultVisualRule } from '@/contexts';

describe('VisualRule Component', () => {
  const index = 1;
  const { colorGrid } = getDefaultVisualRule();
  const nextColor = defaultColor;
  const moveRule = jest.fn();
  const editRule = jest.fn();
  const removeRule = jest.fn();
  const props = {
    index,
    colorGrid,
    nextColor,
    moveRule,
    editRule,
    removeRule,
  };

  afterEach(() => {
    moveRule.mockClear();
    editRule.mockClear();
    removeRule.mockClear();
  });

  test('should render correctly', () => {
    render(<VisualRule {...props} />);

    const visualRule = screen.getByTestId('visual-rule');
    expect(visualRule).toBeInTheDocument();
    expect(visualRule).toHaveTextContent(/Ifthen center cell turns/);
  });

  describe('editRule', () => {
    test('should call editRule correctly if colorGrid is changed', async () => {
      const user = userEvent.setup();
      render(<VisualRule {...props} />);

      // Open color picker and set color
      const colorToSelect = colors[1];
      const firstColorPickerInGrid = screen.getAllByTestId('color-picker')[0];
      await user.click(within(firstColorPickerInGrid).getByRole('button'));
      const colorSelector = await screen.findByTitle(
        colorToSelect.hexValue.toLowerCase()
      );
      expect(colorSelector).toBeInTheDocument();
      await user.click(colorSelector);
      expect(editRule).toHaveBeenCalledTimes(1);
      expect(editRule).toHaveBeenCalledWith(
        {
          colorGrid: [colorToSelect, ...colorGrid.slice(1)],
          nextColor: props.nextColor,
        },
        props.index
      );
    });

    test('should call editRule correctly if nextColor is changed', async () => {
      const user = userEvent.setup();
      render(<VisualRule {...props} />);

      // Open color picker and set color
      const colorToSelect = colors[1];
      const colorPickers = screen.getAllByTestId('color-picker');
      const nextColorPicker = colorPickers[colorPickers.length - 1];
      await user.click(within(nextColorPicker).getByRole('button'));
      const colorSelector = await screen.findByTitle(
        colorToSelect.hexValue.toLowerCase()
      );
      expect(colorSelector).toBeInTheDocument();
      await user.click(colorSelector);
      expect(editRule).toHaveBeenCalledTimes(1);
      expect(editRule).toHaveBeenCalledWith(
        {
          colorGrid,
          nextColor: colorToSelect,
        },
        props.index
      );
    });
  });

  describe('moveRule', () => {
    test('should call moveRule correctly if up button is clicked', async () => {
      const user = userEvent.setup();
      render(<VisualRule {...props} />);

      const moveRuleUpBtn = screen.getByTestId('move-rule-up-btn');
      await user.click(moveRuleUpBtn);
      expect(moveRule).toHaveBeenCalledTimes(1);
      expect(moveRule).toHaveBeenCalledWith(props.index, Direction.Up);
    });

    test('should call moveRule correctly if down button is clicked', async () => {
      const user = userEvent.setup();
      render(<VisualRule {...props} />);

      const moveRuleDownBtn = screen.getByTestId('move-rule-down-btn');
      await user.click(moveRuleDownBtn);
      expect(moveRule).toHaveBeenCalledTimes(1);
      expect(moveRule).toHaveBeenCalledWith(props.index, Direction.Down);
    });
  });

  describe('removeRule', () => {
    test('should call removeRule correctly if remove button is clicked', async () => {
      const user = userEvent.setup();
      render(<VisualRule {...props} />);

      const removeRuleBtn = screen.getByTestId('remove-rule-btn');
      await user.click(removeRuleBtn);
      expect(removeRule).toHaveBeenCalledTimes(1);
      expect(removeRule).toHaveBeenCalledWith(props.index);
    });
  });
});
