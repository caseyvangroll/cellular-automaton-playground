'use client';
import { TextualRule } from './textual-rule';
import { VisualRule } from './visual-rule';
import { RuleType } from '@/types';
import { useRulesContext } from '@/contexts';
import './index.scss';

export function Rules() {
  const {
    rules,
    addTextualRule,
    addVisualRule,
    moveRule,
    editRule,
    removeRule,
  } = useRulesContext();
  return (
    <div id="rules" data-testid="rules">
      <div className="section-header">
        <h3>Rules</h3>
      </div>
      {rules.map((rule: RuleType, index: number) => {
        if ('conditions' in rule) {
          return (
            <TextualRule
              key={index}
              index={index}
              moveRule={moveRule}
              editRule={editRule}
              removeRule={removeRule}
              {...rule}
            />
          );
        }

        return (
          <VisualRule
            key={index}
            index={index}
            moveRule={moveRule}
            editRule={editRule}
            removeRule={removeRule}
            {...rule}
          />
        );
      })}
      <div className="add-buttons">
        <button onClick={addTextualRule} data-testid="add-textual-rule-btn">
          + Textual rule
        </button>
        <button onClick={addVisualRule} data-testid="add-visual-rule-btn">
          + Visual rule
        </button>
      </div>
    </div>
  );
}
