'use client';
import { ColorType, ConditionType } from '@/types';
import { defaultColor } from '@/constants';
import { removeArrayElement, updateArrayElementInPlace } from '@/utils';
import { ColorPicker } from '@/components';
import './conditions.scss';

export interface ConditionProps {
  index: number;
  condition: ConditionType;
  updateCondition: (nextCondition: ConditionType, index: number) => void;
  removeCondition: (event: React.SyntheticEvent<HTMLButtonElement>) => void;
}

export function Condition({
  index,
  condition,
  updateCondition,
  removeCondition,
}: ConditionProps) {
  const setNeighborCt = (event: React.ChangeEvent<HTMLSelectElement>) => {
    updateCondition(
      {
        ...condition,
        neighborCt: parseInt(event.target.value, 10),
      },
      index
    );
  };
  const setColor = (nextColor: ColorType) => {
    updateCondition(
      {
        ...condition,
        neighborColor: nextColor,
      },
      index
    );
  };

  return (
    <div className="condition" data-testid="condition">
      <span>and</span>
      <select
        value={condition.neighborCt}
        onChange={setNeighborCt}
        data-testid="neighbor-ct-select"
      >
        {new Array(8).fill(null).map((_, i) => (
          <option key={i} value={i + 1}>
            {i + 1}
          </option>
        ))}
      </select>
      <span>neighbors are</span>
      <ColorPicker color={condition.neighborColor} setColor={setColor} />
      <button
        data-index={index}
        onClick={removeCondition}
        data-testid="remove-condition-btn"
      >
        x
      </button>
    </div>
  );
}

export interface ConditionsProps {
  conditions: Array<ConditionType>;
  setConditions: (nextConditions: ConditionType[]) => void;
}

export const getDefaultCondition = () => ({
  neighborCt: 1,
  neighborColor: defaultColor,
});

export function Conditions({ conditions, setConditions }: ConditionsProps) {
  const updateCondition = (nextCondition: ConditionType, index: number) => {
    setConditions(updateArrayElementInPlace(conditions, nextCondition, index));
  };

  const addCondition = () =>
    setConditions(conditions.concat([getDefaultCondition()]));

  const removeCondition = (event: React.SyntheticEvent<HTMLButtonElement>) => {
    const { index } = event.currentTarget?.dataset || {};
    if (index !== undefined) {
      setConditions(removeArrayElement(conditions, parseInt(index, 10)));
    }
  };
  const hasMaxConditions = conditions.length >= 5;

  return (
    <div className="conditions" data-testid="conditions">
      {conditions.map((condition, i) => (
        <Condition
          key={i}
          index={i}
          condition={condition}
          updateCondition={updateCondition}
          removeCondition={removeCondition}
        />
      ))}
      {!hasMaxConditions && (
        <button onClick={addCondition} data-testid="add-condition-btn">
          + Condition
        </button>
      )}
    </div>
  );
}
