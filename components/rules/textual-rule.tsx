'use client';
import { memo, useCallback } from 'react';
import { ColorPicker } from '@/components';
import {
  ColorType,
  ConditionType,
  RuleType,
  TextualRuleType,
  Direction,
} from '@/types';
import { Conditions } from './conditions';

// #### Textual Rules:
// - These will allow you to specify general configurations of neighbors (needed for Conway's Game of Life).
// - General Format: `If cell is {color}( and {N neighbors} are {color})* then cell changes to {color}`
//   - The rule `{N neighbors} are {color}` indicates **at least** N neighbors are color, not **exactly** N neighbors are that color.
// - A UI component will assist in creating these, you will not be entering text yourself - you will be clicking buttons and selecting dropdowns.

interface TextualRuleProps extends TextualRuleType {
  index: number;
  moveRule: (index: number, direction: Direction) => void;
  editRule: (rule: RuleType, index: number) => void;
  removeRule: (index: number) => void;
}

export function TextualRuleBase({
  index,
  color,
  conditions,
  nextColor,
  moveRule,
  editRule,
  removeRule,
}: TextualRuleProps) {
  const handleSetColor = useCallback(
    (color: ColorType) =>
      editRule(
        {
          color,
          conditions,
          nextColor,
        },
        index
      ),
    [index, editRule, conditions, nextColor]
  );
  const setConditions = useCallback(
    (conditions: ConditionType[]) =>
      editRule(
        {
          color,
          conditions,
          nextColor,
        },
        index
      ),
    [index, editRule, color, nextColor]
  );
  const handleSetNextColor = useCallback(
    (nextColor: ColorType) =>
      editRule(
        {
          color,
          conditions,
          nextColor,
        },
        index
      ),
    [index, editRule, color, conditions]
  );
  const handleMoveRuleUp = useCallback(
    () => moveRule(index, Direction.Up),
    [index, moveRule]
  );
  const handleMoveRuleDown = useCallback(
    () => moveRule(index, Direction.Down),
    [index, moveRule]
  );
  const handleRemoveRule = useCallback(
    () => removeRule(index),
    [index, removeRule]
  );

  return (
    <div className="rule" data-testid="textual-rule">
      <span className="movement-buttons">
        <button onClick={handleMoveRuleUp} data-testid="move-rule-up-btn">
          ↑
        </button>
        <button onClick={handleMoveRuleDown} data-testid="move-rule-down-btn">
          ↓
        </button>
      </span>
      <div className="textual-rule">
        <span>If cell is</span>
        <ColorPicker
          color={color}
          setColor={handleSetColor}
          allowRainbowColor
        />
        {<Conditions conditions={conditions} setConditions={setConditions} />}
        <span>then cell turns</span>
        <ColorPicker color={nextColor} setColor={handleSetNextColor} />
      </div>
      <button onClick={handleRemoveRule} data-testid="remove-rule-btn">
        x
      </button>
    </div>
  );
}

export const TextualRule = memo(TextualRuleBase);
