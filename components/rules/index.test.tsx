import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Rules } from './index';
import { RulesProvider } from '@/contexts';

describe('Rules Component', () => {
  test('should render correctly', () => {
    render(
      <RulesProvider>
        <Rules />
      </RulesProvider>
    );

    const rules = screen.getByTestId('rules');
    expect(rules).toBeInTheDocument();
    expect(rules).toHaveTextContent(/Rules\+ Textual rule\+ Visual rule/);
  });

  test('should add textual rule when add-textual-rule button is clicked', async () => {
    const user = userEvent.setup();
    render(
      <RulesProvider>
        <Rules />
      </RulesProvider>
    );

    const addTextualRuleBtn = screen.getByTestId('add-textual-rule-btn');
    await user.click(addTextualRuleBtn);
    const textualRule = screen.getByTestId('textual-rule');
    expect(textualRule).toBeInTheDocument();
  });

  test('should add visual rule when add-visual-rule button is clicked', async () => {
    const user = userEvent.setup();
    render(
      <RulesProvider>
        <Rules />
      </RulesProvider>
    );

    const addVisualRuleBtn = screen.getByTestId('add-visual-rule-btn');
    await user.click(addVisualRuleBtn);
    const visualRule = screen.getByTestId('visual-rule');
    expect(visualRule).toBeInTheDocument();
  });
});
