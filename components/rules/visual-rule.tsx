'use client';
import { memo, useCallback } from 'react';
import { ColorPicker } from '@/components';
import { ColorType, RuleType, VisualRuleType, Direction } from '@/types';
import { updateArrayElementInPlace } from '@/utils';

// #### Visual Rules:
// - These will allow you to specify exact configurations of neighbors (needed for definitions similar to those in [Rule 101](https://en.wikipedia.org/wiki/Rule_110#/media/File:One-d-cellular-automaton-rule-110.gif), except in 2d grid).
// - A UI component will assist in creating these, you will be selecting colors from dropdowns in a 9x9 grid and then a next color for the center cell.

interface VisualRuleProps extends VisualRuleType {
  index: number;
  moveRule: (index: number, direction: Direction) => void;
  editRule: (rule: RuleType, index: number) => void;
  removeRule: (index: number) => void;
}

export function VisualRuleBase({
  index,
  colorGrid,
  nextColor,
  moveRule,
  editRule,
  removeRule,
}: VisualRuleProps) {
  const handleSetColorGrid = useCallback(
    (i: number, colorInGrid: ColorType) =>
      editRule(
        {
          colorGrid: updateArrayElementInPlace(colorGrid, colorInGrid, i),
          nextColor,
        },
        index
      ),
    [index, editRule, colorGrid, nextColor]
  );
  const handleSetNextColor = useCallback(
    (nextColor: ColorType) =>
      editRule(
        {
          colorGrid,
          nextColor,
        },
        index
      ),
    [index, editRule, colorGrid]
  );
  const handleMoveRuleUp = useCallback(
    () => moveRule(index, Direction.Up),
    [index, moveRule]
  );
  const handleMoveRuleDown = useCallback(
    () => moveRule(index, Direction.Down),
    [index, moveRule]
  );
  const handleRemoveRule = useCallback(
    () => removeRule(index),
    [index, removeRule]
  );

  return (
    <div className="rule" data-testid="visual-rule">
      <span className="movement-buttons">
        <button onClick={handleMoveRuleUp} data-testid="move-rule-up-btn">
          ↑
        </button>
        <button onClick={handleMoveRuleDown} data-testid="move-rule-down-btn">
          ↓
        </button>
      </span>
      <div className="visual-rule">
        <span>If</span>
        <div className="color-grid">
          {colorGrid.map((color, i) => {
            return (
              <ColorPicker
                key={i}
                color={color}
                setColor={handleSetColorGrid.bind(null, i)}
                allowRainbowColor
              />
            );
          })}
        </div>
        <span>then center cell turns</span>
        <ColorPicker color={nextColor} setColor={handleSetNextColor} />
      </div>
      <button onClick={handleRemoveRule} data-testid="remove-rule-btn">
        x
      </button>
    </div>
  );
}

export const VisualRule = memo(VisualRuleBase);
