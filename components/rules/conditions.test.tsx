import { render, screen } from '@testing-library/react';
import { within } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { Conditions, getDefaultCondition } from './conditions';
import { colors, defaultColor } from '@/constants';

describe('Conditions Component', () => {
  const setConditions = jest.fn();

  afterEach(() => {
    setConditions.mockClear();
  });

  test('should render correctly', () => {
    const conditions = [getDefaultCondition()];
    render(
      <Conditions conditions={conditions} setConditions={setConditions} />
    );

    const conditionsComp = screen.getByTestId('conditions');
    expect(conditionsComp).toBeInTheDocument();

    const conditionComps = screen.getAllByTestId('condition');
    expect(conditionComps).toHaveLength(1);

    const neighborCtSelect = within(conditionComps[0]).getByTestId(
      'neighbor-ct-select'
    );
    expect(neighborCtSelect).toBeInTheDocument();
    expect(neighborCtSelect).toHaveTextContent('12345678');

    const colorPicker = within(conditionComps[0]).getByTestId('color-picker');
    expect(colorPicker).toBeInTheDocument();

    const removeConditionButton = within(conditionComps[0]).getByTestId(
      'remove-condition-btn'
    );
    expect(removeConditionButton).toBeInTheDocument();
    expect(removeConditionButton).toHaveTextContent('x');

    const addConditionBtn = screen.getByTestId('add-condition-btn');
    expect(addConditionBtn).toBeInTheDocument();
    expect(addConditionBtn).toHaveTextContent('+ Condition');
  });

  describe('add condition', () => {
    test('should call setConditions correctly', async () => {
      const user = userEvent.setup();
      const conditions = [getDefaultCondition()];
      render(
        <Conditions conditions={conditions} setConditions={setConditions} />
      );

      const addConditionBtn = screen.getByTestId('add-condition-btn');
      await user.click(addConditionBtn);
      expect(setConditions).toHaveBeenCalledWith([
        ...conditions,
        getDefaultCondition(),
      ]);
    });

    test('should disallow adding conditions when 5 conditions exist', async () => {
      const conditions = [
        getDefaultCondition(),
        getDefaultCondition(),
        getDefaultCondition(),
        getDefaultCondition(),
        getDefaultCondition(),
      ];
      render(
        <Conditions conditions={conditions} setConditions={setConditions} />
      );

      const addConditionBtn = screen.queryByTestId('add-condition-btn');
      expect(addConditionBtn).toEqual(null);
    });
  });

  describe('edit condition', () => {
    test('edit neighborColor - should call setConditions correctly', async () => {
      const user = userEvent.setup();
      const conditions = [getDefaultCondition()];
      render(
        <Conditions conditions={conditions} setConditions={setConditions} />
      );

      const colorPicker = within(screen.getByTestId('color-picker')).getByRole(
        'button'
      );
      await user.click(colorPicker);
      const blackColor =
        colors.find((color) => color.id === 'Black') || defaultColor;
      const blackColorSelector = screen.getByTitle(
        blackColor.hexValue.toLowerCase()
      );
      await user.click(blackColorSelector);
      expect(setConditions).toHaveBeenCalledWith([
        {
          ...conditions[0],
          neighborColor: blackColor,
        },
      ]);
    });

    test('edit neighborCt - should call setConditions correctly', async () => {
      const user = userEvent.setup();
      const conditions = [getDefaultCondition()];
      render(
        <Conditions conditions={conditions} setConditions={setConditions} />
      );

      const neighborCtSelect = screen.getByTestId('neighbor-ct-select');
      const newNeighborCt = '2';
      await user.selectOptions(neighborCtSelect, newNeighborCt);
      expect(setConditions).toHaveBeenCalledWith([
        {
          ...conditions[0],
          neighborCt: Number(newNeighborCt),
        },
      ]);
    });
  });

  describe('remove condition', () => {
    test('should call setConditions correctly', async () => {
      const user = userEvent.setup();
      const conditions = [getDefaultCondition()];
      render(
        <Conditions conditions={conditions} setConditions={setConditions} />
      );

      const removeConditionBtn = screen.getByTestId('remove-condition-btn');
      await user.click(removeConditionBtn);
      expect(setConditions).toHaveBeenCalledWith([]);
    });
  });
});
