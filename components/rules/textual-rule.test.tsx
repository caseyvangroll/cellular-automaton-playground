import { render, screen } from '@testing-library/react';
import { within } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { TextualRule } from './textual-rule';
import { colors, defaultColor } from '@/constants';
import { ConditionType, Direction } from '@/types';
import { getDefaultCondition } from './conditions';

describe('TextualRule Component', () => {
  const index = 1;
  const color = defaultColor;
  const nextColor = defaultColor;
  const conditions: Array<ConditionType> = [];
  const moveRule = jest.fn();
  const editRule = jest.fn();
  const removeRule = jest.fn();
  const props = {
    index,
    color,
    nextColor,
    conditions,
    moveRule,
    editRule,
    removeRule,
  };

  afterEach(() => {
    moveRule.mockClear();
    editRule.mockClear();
    removeRule.mockClear();
  });

  test('should render correctly', () => {
    render(<TextualRule {...props} />);

    const textualRule = screen.getByTestId('textual-rule');
    expect(textualRule).toBeInTheDocument();
    expect(textualRule).toHaveTextContent(/If cell is.+then cell turns/);
  });

  describe('editRule', () => {
    test('should call editRule correctly if color is changed', async () => {
      const user = userEvent.setup();
      render(<TextualRule {...props} />);

      // Open color picker and set color
      const colorToSelect = colors[1];
      const colorPicker = screen.getAllByTestId('color-picker')[0];
      await user.click(within(colorPicker).getByRole('button'));
      const colorSelector = await screen.findByTitle(
        colorToSelect.hexValue.toLowerCase()
      );
      expect(colorSelector).toBeInTheDocument();
      await user.click(colorSelector);
      expect(editRule).toHaveBeenCalledTimes(1);
      expect(editRule).toHaveBeenCalledWith(
        {
          color: colorToSelect,
          conditions: props.conditions,
          nextColor: props.nextColor,
        },
        props.index
      );
    });

    test('should call editRule correctly if condition is added', async () => {
      const user = userEvent.setup();
      render(<TextualRule {...props} />);

      // Open color picker and set color
      const addConditionBtn = screen.getByTestId('add-condition-btn');
      await user.click(addConditionBtn);
      expect(editRule).toHaveBeenCalledTimes(1);
      expect(editRule).toHaveBeenCalledWith(
        {
          color: props.color,
          conditions: props.conditions.concat([getDefaultCondition()]),
          nextColor: props.nextColor,
        },
        props.index
      );
    });

    test('should call editRule correctly if nextColor is changed', async () => {
      const user = userEvent.setup();
      render(<TextualRule {...props} />);

      // Open nextColor picker and set color
      const colorToSelect = colors[1];
      const nextColorPicker = screen.getAllByTestId('color-picker')[1];
      await user.click(within(nextColorPicker).getByRole('button'));
      const colorSelector = await screen.findByTitle(
        colorToSelect.hexValue.toLowerCase()
      );
      expect(colorSelector).toBeInTheDocument();
      await user.click(colorSelector);
      expect(editRule).toHaveBeenCalledTimes(1);
      expect(editRule).toHaveBeenCalledWith(
        {
          color: props.color,
          conditions: props.conditions,
          nextColor: colorToSelect,
        },
        props.index
      );
    });
  });

  describe('moveRule', () => {
    test('should call moveRule correctly if up button is clicked', async () => {
      const user = userEvent.setup();
      render(<TextualRule {...props} />);

      const moveRuleUpBtn = screen.getByTestId('move-rule-up-btn');
      await user.click(moveRuleUpBtn);
      expect(moveRule).toHaveBeenCalledTimes(1);
      expect(moveRule).toHaveBeenCalledWith(props.index, Direction.Up);
    });

    test('should call moveRule correctly if down button is clicked', async () => {
      const user = userEvent.setup();
      render(<TextualRule {...props} />);

      const moveRuleDownBtn = screen.getByTestId('move-rule-down-btn');
      await user.click(moveRuleDownBtn);
      expect(moveRule).toHaveBeenCalledTimes(1);
      expect(moveRule).toHaveBeenCalledWith(props.index, Direction.Down);
    });
  });

  describe('removeRule', () => {
    test('should call removeRule correctly if remove button is clicked', async () => {
      const user = userEvent.setup();
      render(<TextualRule {...props} />);

      const removeRuleBtn = screen.getByTestId('remove-rule-btn');
      await user.click(removeRuleBtn);
      expect(removeRule).toHaveBeenCalledTimes(1);
      expect(removeRule).toHaveBeenCalledWith(props.index);
    });
  });
});
