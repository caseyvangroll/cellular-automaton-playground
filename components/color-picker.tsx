'use client';
import { memo, useState, useMemo, useCallback } from 'react';
import { muffleConsoleErrors } from '@/utils';
import { ColorType } from '@/types';
import { GithubPicker, ColorResult } from 'react-color';
import { colors, defaultColor } from '@/constants';
import './color-picker.scss';

// Get rid of some dumb deprecation warnings
muffleConsoleErrors([
  'Support for defaultProps will be removed from function components in a future major release',
]);

export interface ColorPickerProps {
  color: ColorType;
  setColor:
    | React.Dispatch<React.SetStateAction<ColorType>>
    | ((nextColor: ColorType) => void);
  allowRainbowColor?: boolean;
}

export function ColorPickerBase({
  color,
  setColor,
  allowRainbowColor,
}: ColorPickerProps) {
  const colorOptions = useMemo(
    () =>
      colors
        .map((color) => color.hexValue)
        .filter((hexValue) => allowRainbowColor || hexValue !== 'transparent'),
    [allowRainbowColor]
  );
  const [showColorPicker, setShowColorPicker] = useState(false);

  const toggleShowColorPicker = useCallback(
    () => setShowColorPicker((prevShowColorPicker) => !prevShowColorPicker),
    []
  );

  const handleColorChange = useCallback(
    ({ hex: hexValue }: ColorResult) => {
      setColor(
        colors.find((color) => color.hexValue === hexValue) || defaultColor
      );
    },
    [setColor]
  );

  return (
    <div className="color-picker" data-testid="color-picker">
      <button
        className="selected-color"
        title={color.hexValue}
        style={{ backgroundColor: color.hexValue }}
        onClick={toggleShowColorPicker}
      />
      {showColorPicker && (
        <GithubPicker
          colors={colorOptions}
          onChange={handleColorChange}
          onChangeComplete={toggleShowColorPicker}
          triangle="hide"
          width="1" // Somehow results in making nice and compact
        />
      )}
    </div>
  );
}

export const ColorPicker = memo(ColorPickerBase);
