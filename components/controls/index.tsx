'use client';
import { useCallback } from 'react';
import { useGridContext, useRulesContext } from '@/contexts';
import { ColorPicker } from '../color-picker';

export function Controls() {
  const { step, toolColor, setToolColor } = useGridContext();
  const { rules } = useRulesContext();

  const handleStep = useCallback(() => step(rules), [step, rules]);

  return (
    <div id="controls" className="section-header">
      <button onClick={handleStep}>Step</button>
      <ColorPicker color={toolColor} setColor={setToolColor} />
    </div>
  );
}
