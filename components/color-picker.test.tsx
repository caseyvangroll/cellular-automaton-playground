import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ColorPicker } from './color-picker';
import { defaultColor, colors } from '@/constants';

describe('ColorPicker Component', () => {
  const setColor = jest.fn();

  afterEach(() => {
    setColor.mockClear();
  });

  test('should render', () => {
    render(<ColorPicker color={defaultColor} setColor={setColor} />);

    const colorPicker = screen.getByTestId('color-picker');
    expect(colorPicker).toBeInTheDocument();
  });

  test('should open then close GithubPicker when button is clicked twice', async () => {
    const user = userEvent.setup();
    const { container } = render(
      <ColorPicker color={defaultColor} setColor={setColor} />
    );

    const button = screen.getByRole('button');
    expect(button).toBeInTheDocument();
    await user.click(button);
    expect(container.querySelector('.github-picker')).toBeInTheDocument();
    await user.click(button);
    expect(container.querySelector('.github-picker')).not.toBeInTheDocument();
  });

  test.each(colors)(
    'should close picker and call setColor when %s is selected',
    async (colorToSelect) => {
      const user = userEvent.setup();
      const initialColor =
        colorToSelect === defaultColor ? colors[1] : defaultColor;
      const { container } = render(
        <ColorPicker
          color={initialColor}
          setColor={setColor}
          allowRainbowColor
        />
      );

      const button = screen.getByRole('button');
      expect(button).toBeInTheDocument();
      await user.click(button);
      const colorSelector = screen.getByTitle(
        colorToSelect.hexValue.toLowerCase()
      );
      expect(colorSelector).toBeInTheDocument();
      await user.click(colorSelector);
      expect(setColor).toHaveBeenCalledWith(colorToSelect);
      // The onChangeComplete color picker handler runs slowly
      await waitFor(() => {
        expect(
          container.querySelector('.github-picker')
        ).not.toBeInTheDocument();
      });
    }
  );
});
