'use client';
import { useCallback, useState } from 'react';
import { useRulesContext, useGridContext } from '@/contexts';
import { ConwaysGameOfLife } from './conways-game-of-life';

const presets = [ConwaysGameOfLife];
const defaultPreset = presets[0];

export function Presets() {
  const [preset, setPreset] = useState(defaultPreset);
  const { setRules } = useRulesContext();
  const { setHeight, setWidth, setCellColors } = useGridContext();

  const handlePresetSelect = useCallback(
    (event: React.ChangeEvent<HTMLSelectElement>) => {
      const nextPreset =
        presets.find((p) => p.id === event.target.id) || defaultPreset;
      setPreset(nextPreset);
    },
    []
  );

  const handleLoadPreset = () => {
    setRules(preset.rules);
    setHeight(preset.height);
    setWidth(preset.width);
    setCellColors(preset.cellColors);
  };

  return (
    <div className="section-header">
      <h3>Presets</h3>
      <select
        value={preset.id}
        onChange={handlePresetSelect}
        data-testid="preset-select"
      >
        {presets.map((p) => (
          <option key={p.id} value={p.id}>
            {p.label}
          </option>
        ))}
      </select>
      <button onClick={handleLoadPreset}>Load</button>
    </div>
  );
}
