import { Yellow, Black } from '@/constants';
import { PresetType } from '@/types';

export const ConwaysGameOfLife: PresetType = {
  id: 'conways-game-of-life',
  label: "Conway's Game of Life",
  height: 30,
  width: 30,
  // prettier-ignore
  cellColors: [
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),

    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(13).fill(null).map(() => ({...Black})), Black, Yellow, Black, ...new Array(14).fill(null).map(() => ({...Black})),
    ...new Array(13).fill(null).map(() => ({...Black})), Black, Black, Yellow, ...new Array(14).fill(null).map(() => ({...Black})),
    ...new Array(13).fill(null).map(() => ({...Black})), Yellow, Yellow, Yellow, ...new Array(14).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),

    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
    ...new Array(30).fill(null).map(() => ({...Black})),
  ],
  rules: [
    // - Each cell with one or no neighbors dies, as if by solitude.
    //   - `If cell is {yellow} and {7 neighbors} are {black} then cell changes to {black}`
    {
      color: Yellow,
      conditions: [
        {
          neighborCt: 7,
          neighborColor: Black,
        },
      ],
      nextColor: Black,
    },
    // - Each cell with four or more neighbors dies, as if by overpopulation.
    //   - `If cell is {yellow} and {4 neighbors} are {yellow} then cell changes to {black}`
    {
      color: Yellow,
      conditions: [
        {
          neighborCt: 4,
          neighborColor: Yellow,
        },
      ],
      nextColor: Black,
    },
    // - Each cell with two or three neighbors survives.
    //   - No rule needed - cells will retain color if no rules are true for them.
    // - For a space that is empty or unpopulated: Each cell with three neighbors becomes populated.
    //   - `If cell is {black} and {3 neighbors} are {yellow} and {5 neighbors} are {black} then cell changes to {yellow}`
    {
      color: Black,
      conditions: [
        {
          neighborCt: 3,
          neighborColor: Yellow,
        },
        {
          neighborCt: 5,
          neighborColor: Black,
        },
      ],
      nextColor: Yellow,
    },
  ],
};
