export type ColorType = {
  id: string;
  hexValue: string;
};

export type ConditionType = {
  neighborCt: number;
  neighborColor: ColorType;
};

export type TextualRuleType = {
  color: ColorType;
  conditions: ConditionType[];
  nextColor: ColorType;
};

export type VisualRuleType = {
  colorGrid: ColorType[];
  nextColor: ColorType;
};

export type RuleType = TextualRuleType | VisualRuleType;

export type GridStateType = {
  height: number;
  width: number;
  cellColors: ColorType[];
};

export type PresetType = GridStateType & {
  id: string;
  label: string;
  rules: RuleType[];
};

export type Tool = 'Dropper' | 'Pen';
export enum Direction {
  Up = 'Up',
  Down = 'Down',
}
