import '@testing-library/jest-dom';

// To muffle console errors in tests about canvas not being in JSDom
HTMLCanvasElement.prototype.getContext = () => {};
